# SDG Mapper Keywords



## Getting started

Clone sdgmapper_keywords repository: 
git clone git@code.europa.eu:knowsdgs/sdgmapper_keywords.git

## Support
For support or any feedback please contact us via email: JRC-SDGs@ec.europa.eu or create a new "issue" in this project.

## License
Licensed under the Creative Commons Attribution 4.0 International (CC BY 4.0) licence. Reuse is allowed if provided appropriate credit is given and changes are indicated.

## Project status
SDG Mapper is a project that still receives updates and the keywords may also be subject to change.
